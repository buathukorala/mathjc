@def title = "Welcome"
@def tags = ["syntax", "code"]

#  Learning from Data – our new journal potluck

 
<!-- \tableofcontents  you can use \toc as well -->


![Alt Text](/assets/toronto.gif)




---

## Things to be known:

* **Meeting Time:** Th: 4:30-5:30PM, MATH 014 
    - Live meeting via ZOOM: [Click here!!!](https://zoom.us/j/8065430626)  
* **Organizers:** Drs. Bijoy Ghosh and Bhagya Athukorallage

* **E-mail:** bijoy.ghosh@ttu.edu or bhagya.athukorala@ttu.edu

* [Supplementary files live here!](https://tinyurl.com/yr99hdfk) 

---

## JC lineup


| Date       | Speaker      | Paper Title      |
| :---:       |    :----:   |          :---: |
| 09/02/2021  | Dr. Bijoy Ghosh        | Abstractions, Architecture, Mechanisms, and a Middleware for Networked Control    |
|  09/09/2021 | Dr. Bhagya Athukorallage        | Discovering governing equations from data by sparse identification of nonlinear dynamical systems       |
| 09/16/2021  | Dr. Nanwei Wang      | High-dimensional mixed graphical models with Applications to Genomic Data Integration  |
| 09/23/2021  | Dr. Ruiqi Liu     | Computationally Efficient Classification Algorithm in Posterior Drift Model: Phase Transition and Minimax Adaptivity  |
| 10/07/2021  | Rohana Vithana    | Bistability in a Model of Tumor Immune System Interactions with an Oncolytic Virus Therapy  |
| 10/14/2021  | Dr. Clyde Martin (Horn Professor Emeritus - TTU Math)      | The Mathematics of the Sex Life of Silk Worm Moths [Abstract](https://tinyurl.com/ytkducfc) |
| 10/21/2021  |      |    |
| 10/28/2021  | Dr. Dalton Sakthivadivel (Stony Brook)     |  PDE Analysis via Inference: Treating Dynamical Systems as Data-Generating Processes [Abstract](https://tinyurl.com/4t5u6nt8)  |
| 11/04/2021  |      |    |
| 11/11/2021  |      |    |
| 11/18/2021  |      |    |
| 12/02/2021  | External Speaker     |    |





